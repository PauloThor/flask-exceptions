from flask import Flask, jsonify, request
# import ujson as json
from json import load, dump
import os.path
from operations import logic

app = Flask(__name__)

class UsersError(Exception):
    def __init__(self, message, status, *args, **kwargs):
        super().__init__(message, status, *args, **kwargs)

        self.message = message
        self.status = status

@app.get('/user')
def get_data():
        try:
            if not os.path.isfile('./app/database/database.json'):
                logic.create_database()

            with open("./app/database/database.json", "r") as json_file:
                dados = load(json_file)
                if len(dados) == 0:
                    dados = []

                return {"data": dados}, 200
        except UsersError as err:
            return {"msg": err.message}, 200

        
@app.post('/user')
def post_data():
    data = request.get_json()
    name = data['name']
    email = data['email']

    name_type = type(name).__name__
    email_type = type(email).__name__

    try:
        if (not type(name) == str or not type(email) == str):
            raise UsersError({"wrong fields": [{
                "name": name_type,
                "email": email_type
            }]}, 400)        

        if not os.path.isfile('./app/database/database.json'):
            logic.create_database()

        all_users = []
        id = 0
        with open('./app/database/database.json', "r") as f:
            all_users = load(f)
            length = len(all_users)
            id = 1 if length < 1 else int(all_users[length-1]['id']) + 1


        if len([user for user in all_users if user['email'] == email]) > 0:
            raise UsersError({"msg": "User already exists"}, 409)

        with open('./app/database/database.json', "w") as f:
            user = {"name": name, "email": email, "id": id}

            all_users.append(user)
            dump(all_users, f)
        
            return {"data": all_users}, 201 

    except UsersError as err:
        return err.message, err.status
