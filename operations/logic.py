import os

database_path = os.environ.get('FILES_DIRECTORY')


def create_database():
    os.system('touch ./app/database/database.json')

    with open ('./app/database/database.json', 'w') as f:
        f.write('[]')

